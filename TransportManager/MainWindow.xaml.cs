﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TransportManager.Dialog;
using TransportManager.Model;

namespace TransportManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<POI> POIs { get; set; }
        public ObservableCollection<Vehicle> Vehicles { get; set; }
        private DataService.DataService dataService;
        private CalculationService.CalculationService calculationService;

        public MainWindow()
        {
            InitializeComponent();
            dataService = new DataService.DataService();
            calculationService = new CalculationService.CalculationService();

            POIs = new ObservableCollection<POI>();
            Vehicles = new ObservableCollection<Vehicle>();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            updatePoiGrid();
            updateVehicleGrid();
        }

        private void updatePoiGrid()
        {
            POIs = new ObservableCollection<POI>(dataService.GetAllPOIs());
            poiDataGrid.ItemsSource = POIs;
        }

        private void updateVehicleGrid()
        {
            Vehicles = new ObservableCollection<Vehicle>(dataService.GetAllVehicles());
            vehicleDataGrid.ItemsSource = Vehicles;
        }

        private void poiEditButton_Click(object sender, RoutedEventArgs e)
        {
            if(poiDataGrid.SelectedIndex == -1)
            {
                MessageBox.Show("No item is selected");
                return;
            }

            POI poi = POIs.ElementAt(poiDataGrid.SelectedIndex);
            DialogPOI dlg = new DialogPOI(poi);
            if (dlg.ShowDialog() == true)
            {
                dataService.RemovePOI(poi);
                dataService.AddPOI(poi);
                updatePoiGrid();
            }

        }

        private void poiNewButton_Click(object sender, RoutedEventArgs e)
        {
            POI poi = new POI();
            DialogPOI dlg = new DialogPOI(poi);
            if(dlg.ShowDialog() == true)
            {
                dataService.AddPOI(poi);
                updatePoiGrid();
                calculationService.SimulationEvent(new Model.Events.SimpleEvent());
            }
        }

        private void poiDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (poiDataGrid.SelectedIndex == -1)
            {
                MessageBox.Show("No item is selected");
                return;
            }

            dataService.RemovePOI(POIs.ElementAt(poiDataGrid.SelectedIndex));
            updatePoiGrid();
        }

        private void vehicleEditButton_Click(object sender, RoutedEventArgs e)
        {
            if (vehicleDataGrid.SelectedIndex == -1)
            {
                MessageBox.Show("No item is selected");
                return;
            }

            Vehicle vehicle = Vehicles.ElementAt(vehicleDataGrid.SelectedIndex);
            DialogVehicle dlg = new DialogVehicle(vehicle);
            if (dlg.ShowDialog() == true)
            {
                dataService.RemoveVehicle(vehicle);
                dataService.AddVehicle(vehicle);
                updateVehicleGrid();
            }
        }

        private void vehicleNewButton_Click(object sender, RoutedEventArgs e)
        {
            Vehicle vehicle = new Truck();
            DialogVehicle dlg = new DialogVehicle(vehicle);
            if (dlg.ShowDialog() == true)
            {
                dataService.AddVehicle(vehicle);
                updateVehicleGrid();
                calculationService.SimulationEvent(new Model.Events.SimpleEvent());
            }
        }

        private void vehicleDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (vehicleDataGrid.SelectedIndex == -1)
            {
                MessageBox.Show("No item is selected");
                return;
            }

            dataService.RemoveVehicle(Vehicles.ElementAt(vehicleDataGrid.SelectedIndex));
            updateVehicleGrid();
        }

        private void taskAddButton_Click(object sender, RoutedEventArgs e)
        {
            Model.Task task = new Model.Task();
            DialogTask dlg = new DialogTask(task);
            if (dlg.ShowDialog() == true)
            {
                dataService.AddTask(task);
                calculationService.SimulationEvent(new Model.Events.SimpleEvent());
            }
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            calculationService.StartCalculation();
        }

        private void btnResult_Click(object sender, RoutedEventArgs e)
        {
            DialogDispatch dlg = new DialogDispatch(calculationService);
            dlg.ShowDialog();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            calculationService.StopCalculation();
        }
    }
}
