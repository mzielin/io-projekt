﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TransportManager.Model;

namespace TransportManager.Dialog
{
    /// <summary>
    /// Interaction logic for DialogVehicle.xaml
    /// </summary>
    public partial class DialogVehicle : Window
    {
        private Vehicle _vehicle;

        public DialogVehicle(Vehicle vehicle)
        {
            InitializeComponent();
            _vehicle = vehicle;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitDialog();
        }

        private void InitDialog()
        {
            lblName.Text = _vehicle.Name;
            lblPower.Text = _vehicle.Power.ToString();
            lblFuel.Text = _vehicle.FuelConsumption.ToString();
        }

        private void UpdateData()
        {
            _vehicle.Name = lblName.Text;
            _vehicle.Power = Double.Parse(lblPower.Text);
            _vehicle.FuelConsumption = Double.Parse(lblFuel.Text);
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UpdateData();
                if (_vehicle.Name == "") throw new Exception();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wrong data");
                return;
            }

            DialogResult = true;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
