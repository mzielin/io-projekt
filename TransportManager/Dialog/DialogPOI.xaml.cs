﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TransportManager.Model;

namespace TransportManager.Dialog
{
    /// <summary>
    /// Interaction logic for DialogPOI.xaml
    /// </summary>
    public partial class DialogPOI : Window
    {
        private POI _poi;

        public DialogPOI(POI poi)
        {
            InitializeComponent();
            _poi = poi;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitDialog();
        }

        private void InitDialog()
        {
            lblName.Text = _poi.Name;
            lblDescription.Text = _poi.Description;
            lblLatitude.Text = _poi.Y.ToString();
            lblLongitude.Text = _poi.X.ToString();
        }

        private void UpdateData()
        {
            _poi.Name = lblName.Text;
            _poi.Description = lblDescription.Text;
            _poi.Y = Double.Parse(lblLatitude.Text);
            _poi.X = Double.Parse(lblLongitude.Text);
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UpdateData();
                if (_poi.Name == "") throw new Exception();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Wrong data");
                return;
            }

            DialogResult = true;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        



    }
}
