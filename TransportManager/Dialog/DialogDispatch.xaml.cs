﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TransportManager.Model;

namespace TransportManager.Dialog
{
    /// <summary>
    /// Interaction logic for DialogDispatch.xaml
    /// </summary>
    public partial class DialogDispatch : Window
    {
        private CalculationService.CalculationService _calculationService;
        private DateTime _lastUpdate;
        private Thread _checkThread;
        private IEnumerable<Dispatch> _dispatchs;
        private int selected;
        private Dispatch _dispatch;

        public DialogDispatch(CalculationService.CalculationService calculationService)
        {
            InitializeComponent();
            _calculationService = calculationService;
            _dispatchs = new List<Dispatch>();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //webBrowser.Navigate(new Uri(@"C:\Users\Matt\Documents\Visual Studio 2013\Projects\TransportManager\TransportManager\map.html", UriKind.Absolute));
            webBrowser.Navigate(new Uri("C:\\Users\\pinki_000\\Source\\Repos\\io-projekt\\TransportManager\\map.html", UriKind.Absolute));

            UpdateResult();
            _checkThread = new Thread(CheckUpdate);
            _checkThread.Start();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if(_checkThread != null)
            {
                _checkThread.Abort();
            }
        }

        private void UpdateResult()
        {
            Dispatch disp = GetDispatch();
            _dispatch = disp;
            if(disp != null)
            {
                listRoute.ItemsSource = null;
                listRoute.ItemsSource = disp.Route.Itinerary;

                lblName.Content = "Name: " + disp.Vehicle.Name;
                lblPower.Content = "Power: " + disp.Vehicle.Power.ToString();
                lblFuel.Content = "FuelConsumption: " + disp.Vehicle.FuelConsumption.ToString();
            }
            else
            {
                ClearAll();
            }
            UpdateNavi();
        }

        private Dispatch GetDispatch()
        {
            try
            {
                return _dispatchs.ElementAt(selected);
            }
            catch(Exception e)
            {
                selected = 0;
            }

            try
            {
                return _dispatchs.ElementAt(selected);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private void ClearAll()
        {
            listRoute.ItemsSource = null;
            lblName.Content = "Name: ";
            lblPower.Content = "Power: ";
            lblFuel.Content = "FuelConsumption: ";
        }

        private void UpdateNavi()
        {
            if(_dispatchs.Count() != 0)
            {
                navLabel.Content = (selected + 1).ToString() + "/" + _dispatchs.Count().ToString();
            }
            else
            {
                navLabel.Content = "0/0";
            }
        }

        public void CheckUpdate()
        {
            while(true)
            {
                if(_lastUpdate < _calculationService.TimeStamp)
                {
                    _lastUpdate = DateTime.Now;
                    _dispatchs = _calculationService.GetCurrentResult();        
                    listRoute.Dispatcher.Invoke(new Action(() => UpdateResult()));
                }
                Thread.Sleep(1000);
            }
        }

        private void ShowRoute(POI from, POI to)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
            webBrowser.InvokeScript("calcRoute", Math.Round(from.Y, 6).ToString("F6", culture), Math.Round(from.X, 6).ToString("F6", culture), Math.Round(to.Y, 6).ToString("F6", culture), Math.Round(to.X, 6).ToString("F6", culture));   
        }

        private void navBtnPrev_Click(object sender, RoutedEventArgs e)
        {
            selected = _dispatchs.Count() > 0 ? _dispatchs.Count() - 1 : 0;
            UpdateResult();
        }

        private void navBtnNext_Click(object sender, RoutedEventArgs e)
        {
            selected = (selected + 1) % _dispatchs.Count();
            UpdateResult();
        }

        private void listRoute_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(listRoute.SelectedIndex != -1)
            {
                POI poi = listRoute.SelectedItem as POI;
                gridActions.ItemsSource = _dispatch.Route.Actions.Where(x => x.Place.Name == poi.Name);

                int i = listRoute.SelectedIndex;

                if (_dispatch == null || _dispatch.Route == null || _dispatch.Route.Itinerary == null || _dispatch.Route.Itinerary.Count < 2)
                    return;

                if (i == 0) i++;

                ShowRoute(_dispatch.Route.Itinerary.ElementAt(i - 1), _dispatch.Route.Itinerary.ElementAt(i));
            }

        }


    }
}
