﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TransportManager.Model;

namespace TransportManager.Dialog
{
    /// <summary>
    /// Interaction logic for DialogTask.xaml
    /// </summary>
    public partial class DialogTask : Window
    {
        private Model.Task _task;
        private List<POI> pois;
        private List<String> poiNames;
        private DataService.DataService dataService = new DataService.DataService();

        public DialogTask(Model.Task task)
        {
            InitializeComponent();
            _task = task;

            pois = new List<POI>(dataService.GetAllPOIs());
            poiNames = new List<string>(pois.Select(x => x.Name));
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitDialog();
        }

        private void InitDialog()
        {
            cbFrom.ItemsSource = poiNames;
            cbTo.ItemsSource = poiNames;
        }

        private void UpdateData()
        {
            POI from = pois.First(x => x.Name == cbFrom.SelectedItem.ToString());
            Client clientFrom = new Client();
            clientFrom.Address = from;
            _task.Sender = clientFrom;
            POI to = pois.First(x => x.Name == cbTo.SelectedItem.ToString());
            Client clietTo = new Client();
            clietTo.Address = to;
            _task.Receiver = clietTo;

            _task.Description = lblDescription.Text;

            Cargo cargo = new Cargo();
            cargo.Id = lblCargoID.Text;
            cargo.Weight = Double.Parse(lblWeight.Text);

            _task.Cargo = cargo;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UpdateData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wrong data");
                return;
            }

            DialogResult = true;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
