﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportManager.Model
{
    public class CargoAction
    {
        public CargoActionType Type { get; set; }
        public POI Place { get; set; }
        public Cargo Cargo { get; set; }
    }
}
