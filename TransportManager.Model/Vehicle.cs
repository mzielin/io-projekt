﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TransportManager.Model
{
    [XmlInclude(typeof(Truck))]
    public abstract class Vehicle : BaseModel
    {
        public String Name { get; set; }
        public double Power { get; set; }
        public double FuelConsumption { get; set; }
        public int Relability { get; set; }
        public int Convenience { get; set; }
        public string ReqiuredLicense { get; set; }
        public bool IsOperational { get; set; }
    }
}
