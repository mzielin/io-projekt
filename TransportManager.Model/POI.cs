﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportManager.Model
{
    public class POI : BaseModel
    {
        public string Name { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public POICategory  Category{ get; set; }
        public string Description { get; set; }
    }
}
