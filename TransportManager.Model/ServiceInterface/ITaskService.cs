﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TransportManager.Model.Events;


namespace TransportManager.Model.ServiceInterface
{
    [ServiceContract]
    interface ITaskService
    {
        [OperationContract]
        void SimulationEvent(ISimulationEvent e);

        [OperationContract]
        DateTime GetLastResultUpdate();
        [OperationContract]
        void SetLastResultUpdate(DateTime d); 
        [OperationContract]
        IEnumerable<Dispatch> GetCurrentResult();
        
        [OperationContract]
        ServiceAddres GetActiveEngine();
        [OperationContract]
        void SetActiveEngine(ServiceAddres a);
        [OperationContract]
        void RegisterEngine(ServiceAddres a);
        [OperationContract]
        void UnregisterEngine(ServiceAddres a);
        [OperationContract]
        IEnumerable<ServiceAddres> GetAllReadyEngines();
    }
}
