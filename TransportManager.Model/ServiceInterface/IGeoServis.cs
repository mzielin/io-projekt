﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace TransportManager.Model.ServiceInterface
{
    [ServiceContract]
    interface IGeoServis
    {
        [OperationContract]
        Double GetDistance(POI p1, POI p2);
        [OperationContract]
        Dictionary<POI, Dictionary<POI, Double>> GetDistanceGraph(IEnumerable<POI> poiList);
    }
}
