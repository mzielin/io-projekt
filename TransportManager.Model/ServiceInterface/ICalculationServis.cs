﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TransportManager.Model.Events;

namespace TransportManager.Model.ServiceInterface
{
    [ServiceContract]
    public interface ICalculationServis
    {
        [OperationContract]
        void SimulationEvent(ISimulationEvent e);
        [OperationContract]
        IEnumerable<Dispatch> GetCurrentResult();
    }
}
