﻿using System.Collections.Generic;
using System.ServiceModel;
using TransportManager.Model;

namespace TransportManager.DataService
{
    [ServiceContract]
    public interface IDataService
    {
        [OperationContract]
        POI AddPOI(POI poi);
        [OperationContract]
        IEnumerable<POI> GetAllPOIs();
        [OperationContract]
        void RemovePOI(POI poi);
        [OperationContract]
        Client AddClient(Client client);
        [OperationContract]
        IEnumerable<Client> GetAllClients();
        [OperationContract]
        Client GetClientById(string id);
        [OperationContract]
        void RemoveClient(Client client);
        [OperationContract]
        Dispatch AddDispatch(Dispatch dispatch);
        [OperationContract]
        IEnumerable<Dispatch> GetAllDispatches();
        [OperationContract]
        void RemoveDispatch(Dispatch dispatch);
        [OperationContract]
        Driver AddDriver(Driver driver);
        [OperationContract]
        IEnumerable<Driver> GetAllDrivers();
        [OperationContract]
        Driver GetDriverById(string id);
        [OperationContract]
        void RemoveDriver(Driver driver);
        [OperationContract]
        Warehouse AddWarehouse(Warehouse warehouset);
        [OperationContract]
        IEnumerable<Warehouse> GetAllWarehouses();
        [OperationContract]
        void RemoveWarehouse(Warehouse warehouse);
        [OperationContract]
        Vehicle AddVehicle(Vehicle vehicle);
        [OperationContract]
        IEnumerable<Vehicle> GetAllVehicles();
        [OperationContract]
        void RemoveVehicle(Vehicle vehicle);
        [OperationContract]
        Trailer AddTrailer(Trailer trailer);
        [OperationContract]
        IEnumerable<Trailer> GetAllTrailers();
        [OperationContract]
        void RemoveTrailer(Trailer trailer);
        [OperationContract]
        Task AddTask(Task task);
        [OperationContract]
        IEnumerable<Task> GetAllTasks();
        [OperationContract]
        Task GetTaskById(string id);
        [OperationContract]
        void RemoveTask(Task task);
    }

}
