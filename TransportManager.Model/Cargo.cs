﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportManager.Model
{
    public class Cargo
    {
        public String Id { get; set; }
        public String Description { get; set; }
        public double Weight { get; set; }
        public Dimensions Dimensions { get; set; }
    }
}
