﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportManager.Model
{
    public class Driver : BaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Licenses { get; set; }
        public bool IsAvailable { get; set; }
    }
}
