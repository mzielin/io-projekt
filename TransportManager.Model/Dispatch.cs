﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportManager.Model
{
    public class Dispatch : BaseModel
    {
        public String Id { get; set; }
        public Driver MainDriver { get; set; }
        public List<Driver> OtherDrivers { get; set; }
        public Route Route { get; set; }
        public Vehicle Vehicle { get; set; }

    }
}
