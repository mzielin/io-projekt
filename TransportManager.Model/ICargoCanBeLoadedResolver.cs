﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportManager.Model.Interfaces
{
    public interface ICargoCanBeLoadedResolver
    {
        bool CanCargoBeLoaded(ICargoLoader loader, Cargo cargo);
    }
}
