﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportManager.Model
{
    public class DispatchState
    {
        public Dispatch Dispatch { get; set; }
        public List<Cargo> Load { get; set; }
        public POI CurrentTarget { get; set; }
    }
}
