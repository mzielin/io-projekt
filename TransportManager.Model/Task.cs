﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportManager.Model
{
    public class Task : BaseModel
    {
        public String Id { get; set; }
        public String Description { get; set; }
        public Cargo Cargo { get; set; }
        public Client Sender { get; set; }
        public Client Receiver { get; set; }
        public DateTime StartTerm { get; set; }
        public DateTime EndTerm { get; set; }
        public Status Status { get; set; }
    }
}
