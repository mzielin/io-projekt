﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.Model.Interfaces;

namespace TransportManager.Model
{
    public abstract class Trailer :  BaseModel, ICargoLoader
    {
        public Dimensions Dimensions { get; set; }
        public double Weight { get; set; }
        public double OwnWeight { get; set; }
    }
}
