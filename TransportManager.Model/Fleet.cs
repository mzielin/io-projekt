﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportManager.Model
{
    public class Fleet
    {
        public List<Vehicle> VechicleList { get; set; }
        public List<Trailer> TrailersList { get; set; }
    }
}
