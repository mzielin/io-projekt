﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportManager.Model
{
    public abstract class Tractor : Vehicle
    {
        public Trailer Trailer { get; set; }
    }
}
