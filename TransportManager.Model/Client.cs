﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportManager.Model
{
    public class Client : BaseModel
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public POI Address { get; set; }
    }
}
