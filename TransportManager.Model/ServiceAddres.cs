﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportManager.Model
{
    public class ServiceAddres
    {
        public String Addres { get; set; }
        public UInt16 Port { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
    }
}
