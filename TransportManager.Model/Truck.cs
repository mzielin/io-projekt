﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.Model.Interfaces;

namespace TransportManager.Model
{
    public class Truck : Vehicle, ICargoLoader
    {
        public Dimensions Dimensions { get; set; }
    }
}
