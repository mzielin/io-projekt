﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;
using TransportManager.Model;
using TransportManager.Model.ServiceInterface;
using TransportManager.DataService;
namespace TransportManager.CalculationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class CalculationService : ICalculationServis
    {
        private POI START_LOCATION = new POI()
        {
            DB_ID = 0,
            Name = "Warehouse",
            Description = "Warehouse",
            Y = 50.082408,
            X = 19.877808
        };
        private object lockObject = new object();
        private Random rand = new Random();
        private DataService.DataService dataService;
        private List<Dispatch> activeDispatches = new List<Dispatch>();
        private List<Task> tasksInProgress = new List<Task>();
        private List<Vehicle> usedVehicles = new List<Vehicle>();
        private Thread simThread;
        public CalculationService()
        {
            dataService = new DataService.DataService();
        }
        public DateTime TimeStamp
        {
            get;
            protected set;
        }
        public void SimulationEvent(Model.Events.ISimulationEvent e)
        {
            lock (lockObject)
            {
                ReCalculate();
            }
        }
        public void StartCalculation()
        {
            simThread = new Thread(new ThreadStart(Simulation));
            simThread.Start();
        }
        public void StopCalculation()
        {
            simThread.Abort();
        }

        public IEnumerable<Model.Dispatch> GetCurrentResult()
        {
            List<Dispatch> list;
            lock(lockObject)
            {
                list = activeDispatches.ToList();
            }
            return list;
        }

        private void Simulation()
        {
            lock (lockObject)
            {
                ReCalculate();
            }
            while (true)
            {
                Thread.Sleep(6000);
                
                lock(lockObject)
                {
                    List<Dispatch> toRemove = new List<Dispatch>();
                    foreach (var dispatch in activeDispatches)
                    {
                        if (rand.NextDouble() > 0.75)
                        {
                            dispatch.Route.Actions.RemoveAt(0);
                            dispatch.Route.Itinerary.RemoveAt(0);
                            if (dispatch.Route.Actions.Count == 0)
                            {
                                toRemove.Add(dispatch);
                            }
                        }
                    }
                    toRemove.ForEach(disp => usedVehicles.Remove(disp.Vehicle));
                    toRemove.ForEach(disp => activeDispatches.Remove(disp));
                    toRemove.ForEach(disp => dataService.RemoveDispatch(disp));
                    TimeStamp = DateTime.Now;
                }

            }


        }

        private void ReCalculate()
        {
            IEnumerable<Task> tasks = dataService.GetAllTasks();
            IEnumerable<Vehicle> vehicles = dataService.GetAllVehicles();

            List<Task> newtasks = new List<Task>();
            foreach (var task in tasks)
            {
                if (tasksInProgress.FirstOrDefault(t => t.DB_ID == task.DB_ID) == null)
                {
                    newtasks.Add(task);
                }
            }

            List<Vehicle> newVehicles = new List<Vehicle>();
            foreach (var vehicle in vehicles)
            {
                if (usedVehicles.FirstOrDefault(t => t.DB_ID == vehicle.DB_ID) == null)
                {
                    newVehicles.Add(vehicle);
                }
            }

            while(newtasks.Count > 0 && newVehicles.Count >0)
            {
                Vehicle vehicle = newVehicles.First(); newVehicles.RemoveAt(0);
                Task task = newtasks.First(); newtasks.RemoveAt(0);
                Dispatch newDispatch = new Dispatch()
                    {
                        Vehicle = vehicle,
                        Route = new Route()
                    };
                List<CargoAction> cargoactions = new List<CargoAction>();
                List<POI> pois = new List<POI>();

                pois.Add(START_LOCATION);
                pois.Add(task.Sender.Address);
                pois.Add(task.Receiver.Address);

                cargoactions.Add(new CargoAction()
                {
                    Cargo = null,
                    Type = CargoActionType.Start,
                    Place = START_LOCATION
                });
                cargoactions.Add(new CargoAction()
                {
                    Cargo = task.Cargo,
                    Type = CargoActionType.Loading,
                    Place = task.Sender.Address
                });
                
                cargoactions.Add(new CargoAction()
                {
                    Cargo = task.Cargo,
                    Type = CargoActionType.Unloading,
                    Place = task.Receiver.Address
                });
                newDispatch.Route.Actions = cargoactions;
                newDispatch.Route.Itinerary = pois;

                usedVehicles.Add(vehicle);
                //tasksInProgress.Add(task);
                activeDispatches.Add(newDispatch);
                dataService.RemoveTask(task);
                //tasksInProgress.Remove(task);
            }

            while (newtasks.Count > 0 && activeDispatches.Count > 0)
            {
                Task task = newtasks.First(); newtasks.RemoveAt(0);
                var dispatch = activeDispatches.Where(d => vehicles.FirstOrDefault(v => v.DB_ID == d.Vehicle.DB_ID)!= null).OrderBy(disp => disp.Route.Actions.Count).FirstOrDefault();

                if (dispatch.Route.Itinerary.Count > 0 && dispatch.Route.Itinerary.Last().DB_ID == task.Sender.Address.DB_ID)
                {


                }
                else
                {
                    dispatch.Route.Itinerary.Add(task.Sender.Address);
                    dispatch.Route.Actions.Add(new CargoAction()
                    {
                        Cargo = task.Cargo,
                        Type = CargoActionType.Loading,
                        Place = task.Sender.Address
                    });
                }
                //dispatch.Route.Itinerary.Add(task.Sender.Address);
                dispatch.Route.Itinerary.Add(task.Receiver.Address);
                //dispatch.Route.Actions.Add(new CargoAction()
                //{
                //    Cargo = task.Cargo,
                //    Type = CargoActionType.Loading,
                //    Place = task.Sender.Address
                //});
                dispatch.Route.Actions.Add(new CargoAction()
                {
                    Cargo = task.Cargo,
                    Type = CargoActionType.Unloading,
                    Place = task.Receiver.Address
                });
                //tasksInProgress.Add(task);
                dataService.RemoveTask(task);
                //tasksInProgress.Remove(task);
            }
            TimeStamp = DateTime.Now;
        }
    }
}
