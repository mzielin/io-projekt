﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.Model;

namespace TransportManager.DataService.Interfaces
{
    public interface IDispatchRepository
    {
        Dispatch AddDispatch(Dispatch dispatch);
        IEnumerable<Dispatch> GetAllDispatches();
        void RemoveDispatch(Dispatch dispatch);
    }
}
