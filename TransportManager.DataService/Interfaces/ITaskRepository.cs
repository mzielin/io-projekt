﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransportManager.Model;

namespace TransportManager.DataService.Interfaces
{
    public interface ITaskRepository
    {
        Task AddTask(Task task);
        IEnumerable<Task> GetAllTasks();
        Task GetTaskById(string id);
        void RemoveTask(Task task);
    }
}
