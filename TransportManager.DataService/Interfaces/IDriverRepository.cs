﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.Model;

namespace TransportManager.DataService.Interfaces
{
    public interface IDriverRepository
    {
        Driver AddDriver(Driver driver);
        IEnumerable<Driver> GetAllDrivers();
        Driver GetDriverById(string id);
        void RemoveDriver(Driver driver);
    }
}
