﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.Model;

namespace TransportManager.DataService.Interfaces
{
    public interface IVehicleRepository
    {
        Vehicle AddVehicle(Vehicle vehicle);
        IEnumerable<Vehicle> GetAllVehicles();
        void RemoveVehicle(Vehicle vehicle);
    }
}
