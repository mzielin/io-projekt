﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.Model;

namespace TransportManager.DataService.Interfaces
{
    public interface IWarehouseRepository
    {
        Warehouse AddWarehouse(Warehouse warehouset);
        IEnumerable<Warehouse> GetAllWarehouses();
        void RemoveWarehouse(Warehouse warehouse);
    }
}
