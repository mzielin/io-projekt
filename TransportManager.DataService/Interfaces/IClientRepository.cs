﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.Model;

namespace TransportManager.DataService.Interfaces
{
    public interface IClientRepository
    {
        Client AddClient(Client client);
        IEnumerable<Client> GetAllClients();
        Client GetClientById(string id);
        void RemoveClient(Client client);
    }
}
