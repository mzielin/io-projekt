﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.Model;

namespace TransportManager.DataService.Interfaces
{
    public interface ITrailerRepository
    {
        Trailer AddTrailer(Trailer trailer);
        IEnumerable<Trailer> GetAllTrailers();
        void RemoveTrailer(Trailer trailer);
    }
}
