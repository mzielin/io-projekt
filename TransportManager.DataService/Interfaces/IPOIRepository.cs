﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.Model;

namespace TransportManager.DataService.Interfaces
{
    public interface IPOIRepository
    {
        POI AddPOI(POI poi);
        IEnumerable<POI> GetAllPOIs();
        void RemovePOI(POI poi);
    }
}
