﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.DataService.Interfaces;
using TransportManager.DataService.Utils;
using TransportManager.Model;

namespace TransportManager.DataService.Implementations
{
    public class DispatchRepository : IDispatchRepository
    {
        private const string STORAGE_PATH = "dispatches.xml";
        private XMLDataManager storage;
        public DispatchRepository()
        {
            storage = new XMLDataManager(STORAGE_PATH);
            //storage.Save<List<Dispatch>>(new List<Dispatch>());
        }


        public Dispatch AddDispatch(Dispatch dispatch)
        {
            int id = 1;
            var coll = GetAllDispatches();
            if (coll.Count() > 0) id = coll.Max(cl => cl.DB_ID) + 1;
            dispatch.DB_ID = id;
            storage.AddSingle<Dispatch>(dispatch);
            return dispatch;
        }

        public IEnumerable<Dispatch> GetAllDispatches()
        {
            return storage.Load<List<Dispatch>>();
        }

        public void RemoveDispatch(Dispatch dispatch)
        {
            var elements = storage.Load<List<Dispatch>>().ToList();
            var elementToRemove = elements.FirstOrDefault(di => di.DB_ID == dispatch.DB_ID );
            elements.Remove(elementToRemove);
            storage.Save<List<Dispatch>>(elements);
        }
    }
}
