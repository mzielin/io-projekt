﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.DataService.Interfaces;
using TransportManager.DataService.Utils;
using TransportManager.Model;

namespace TransportManager.DataService.Implementations
{
    public class ClientRepository : IClientRepository
    {
        private const string STORAGE_PATH = "clients.xml";
        private XMLDataManager storage;
        public ClientRepository()
        {
            storage = new XMLDataManager(STORAGE_PATH);
            //storage.Save<List<Client>>(new List<Client>());
        }
        public Model.Client AddClient(Model.Client client)
        {
            int id = 1;
            var clients = GetAllClients();
            if(clients.Count() > 0) id = clients.Max(cl => cl.DB_ID) + 1;
            client.DB_ID = id;
            storage.AddSingle<Client>(client);
            return client;
        }

        public IEnumerable<Model.Client> GetAllClients()
        {
            return storage.Load<List<Client>>();
        }

        public Model.Client GetClientById(string id)
        {
            return storage.Load<List<Client>>().FirstOrDefault(cl => cl.Id.Equals(id));
        }

        public void RemoveClient(Model.Client client)
        {
            var elements = storage.Load<List<Client>>().ToList();
            var elementToRemove = elements.FirstOrDefault(cl => cl.DB_ID == client.DB_ID);
            elements.Remove(elementToRemove);
            storage.Save<List<Client>>(elements);
        }
    }
}
