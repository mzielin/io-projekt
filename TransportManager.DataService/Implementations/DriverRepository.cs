﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.DataService.Interfaces;
using TransportManager.DataService.Utils;
using TransportManager.Model;

namespace TransportManager.DataService.Implementations
{
    public class DriverRepository : IDriverRepository
    {
        private const string STORAGE_PATH = "drivers.xml";
        private XMLDataManager storage;
        public DriverRepository()
        {
            storage = new XMLDataManager(STORAGE_PATH);
            //storage.Save<List<Driver>>(new List<Driver>());
        }
        public Driver AddDriver(Model.Driver driver)
        {
            int id = 1;
            var coll = GetAllDrivers();
            if (coll.Count() > 0) id = coll.Max(cl => cl.DB_ID) + 1;
            driver.DB_ID = id;
            storage.AddSingle<Driver>(driver);
            return driver;
        }

        public IEnumerable<Model.Driver> GetAllDrivers()
        {
            return storage.Load<List<Driver>>();
        }

        public Model.Driver GetDriverById(string id)
        {
            return storage.Load<List<Driver>>().FirstOrDefault(cl => cl.Id.Equals(id));
        }

        public void RemoveDriver(Model.Driver driver)
        {
            var elements = storage.Load<List<Driver>>().ToList();
            var elementToRemove = elements.FirstOrDefault(cl => cl.DB_ID == driver.DB_ID);
            elements.Remove(elementToRemove);
            storage.Save<List<Driver>>(elements);
        }
    }
}
