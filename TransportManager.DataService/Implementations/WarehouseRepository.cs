﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.DataService.Interfaces;
using TransportManager.DataService.Utils;
using TransportManager.Model;

namespace TransportManager.DataService.Implementations
{
    public class WarehouseRepository : IWarehouseRepository
    {
        private const string STORAGE_PATH = "warehouses.xml";
        private XMLDataManager storage;
        public WarehouseRepository()
        {
            storage = new XMLDataManager(STORAGE_PATH);
            //storage.Save<List<Warehouse>>(new List<Warehouse>());
        }

        public Warehouse AddWarehouse(Model.Warehouse warehouset)
        {
            int id = 1;
            var coll = GetAllWarehouses();
            if (coll.Count() > 0) id = coll.Max(cl => cl.DB_ID) + 1;
            warehouset.DB_ID = id;
            storage.AddSingle<Warehouse>(warehouset);
            return warehouset;
        }

        public IEnumerable<Model.Warehouse> GetAllWarehouses()
        {
            return storage.Load<List<Warehouse>>();
        }

        public void RemoveWarehouse(Model.Warehouse warehouse)
        {
            var elements = storage.Load<List<Warehouse>>().ToList();
            var elementToRemove = elements.FirstOrDefault(cl => cl.DB_ID == warehouse.DB_ID);
            elements.Remove(elementToRemove);
            storage.Save<List<Warehouse>>(elements);
        }
    }
}
