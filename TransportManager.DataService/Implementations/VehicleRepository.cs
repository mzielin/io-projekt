﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.DataService.Interfaces;
using TransportManager.DataService.Utils;
using TransportManager.Model;

namespace TransportManager.DataService.Implementations
{
    public class VehicleRepository : IVehicleRepository
    {
        private const string STORAGE_PATH = "vehicles.xml";
        private XMLDataManager storage;
        public VehicleRepository()
        {
            storage = new XMLDataManager(STORAGE_PATH);
            //storage.Save<List<Vehicle>>(new List<Vehicle>());
        }

        public Vehicle AddVehicle(Model.Vehicle vehicle)
        {
            int id = 1;
            var coll = GetAllVehicles();
            if (coll.Count() > 0) id = coll.Max(cl => cl.DB_ID) + 1;
            vehicle.DB_ID = id;
            storage.AddSingle<Vehicle>(vehicle);
            return vehicle;
        }

        public IEnumerable<Model.Vehicle> GetAllVehicles()
        {
            return storage.Load<List<Vehicle>>();
        }

        public void RemoveVehicle(Model.Vehicle vehicle)
        {
            var elements = storage.Load<List<Vehicle>>().ToList();
            var elementToRemove = elements.FirstOrDefault(cl => cl.DB_ID == vehicle.DB_ID);
            elements.Remove(elementToRemove);
            storage.Save<List<Vehicle>>(elements);
        }
    }
}
