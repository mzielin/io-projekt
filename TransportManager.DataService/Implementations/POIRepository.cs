﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.DataService.Interfaces;
using TransportManager.DataService.Utils;
using TransportManager.Model;

namespace TransportManager.DataService.Implementations
{
    public class POIRepository : IPOIRepository
    {
        private const string STORAGE_PATH = "pois.xml";
        private XMLDataManager storage;
        public POIRepository()
        {
            storage = new XMLDataManager(STORAGE_PATH);
            //storage.Save<List<POI>>(new List<POI>());
        }
        public Model.POI AddPOI(Model.POI poi)
        {
            int id = 1;
            var clients = GetAllPOIs();
            if (clients.Count() > 0) id = clients.Max(cl => cl.DB_ID) + 1;
            poi.DB_ID = id;
            storage.AddSingle<POI>(poi);
            return poi;
        }

        public IEnumerable<Model.POI> GetAllPOIs()
        {
            return storage.Load<List<POI>>();
        }

        public void RemovePOI(Model.POI poi)
        {
            var elements = storage.Load<List<POI>>().ToList();
            var elementToRemove = elements.FirstOrDefault(cl => cl.DB_ID == poi.DB_ID);
            elements.Remove(elementToRemove);
            storage.Save<List<POI>>(elements);
        }
    }
}
