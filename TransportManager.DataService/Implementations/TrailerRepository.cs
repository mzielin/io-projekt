﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportManager.DataService.Interfaces;
using TransportManager.DataService.Utils;
using TransportManager.Model;

namespace TransportManager.DataService.Implementations
{
    public class TrailerRepository : ITrailerRepository
    {
        private const string STORAGE_PATH = "trailers.xml";
        private XMLDataManager storage;
        public TrailerRepository()
        {
            storage = new XMLDataManager(STORAGE_PATH);
            //storage.Save<List<Trailer>>(new List<Trailer>());
        }

        public Trailer AddTrailer(Model.Trailer trailer)
        {
            int id = 1;
            var coll = GetAllTrailers();
            if (coll.Count() > 0) id = coll.Max(cl => cl.DB_ID) + 1;
            trailer.DB_ID = id;
            storage.AddSingle<Trailer>(trailer);
            return trailer;
        }

        public IEnumerable<Model.Trailer> GetAllTrailers()
        {
            return storage.Load<List<Trailer>>();
        }

        public void RemoveTrailer(Model.Trailer trailer)
        {
            var elements = storage.Load<List<Trailer>>().ToList();
            var elementToRemove = elements.FirstOrDefault(cl => cl.DB_ID == trailer.DB_ID);
            elements.Remove(elementToRemove);
            storage.Save<List<Trailer>>(elements);
        }
    }
}
