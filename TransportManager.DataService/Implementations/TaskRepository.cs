﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransportManager.DataService.Interfaces;
using TransportManager.DataService.Utils;
using TransportManager.Model;

namespace TransportManager.DataService.Implementations
{
    public class TaskRepository : ITaskRepository
    {
        private const string STORAGE_PATH = "tasks.xml";
        private XMLDataManager storage;
        public TaskRepository()
        {
            storage = new XMLDataManager(STORAGE_PATH);
            //storage.Save<List<Task>>(new List<Task>());
        }

        public Task AddTask(Model.Task task)
        {
            int id = 1;
            var coll = GetAllTasks();
            if (coll.Count() > 0) id = coll.Max(cl => cl.DB_ID) + 1;
            task.DB_ID = id;
            storage.AddSingle<Task>(task);
            return task;
        }

        public IEnumerable<Model.Task> GetAllTasks()
        {
            return storage.Load<List<Task>>();
        }

        public Model.Task GetTaskById(string id)
        {
            return storage.Load<List<Task>>().FirstOrDefault(cl => cl.Id.Equals(id));
        }

        public void RemoveTask(Model.Task task)
        {
            var elements = storage.Load<List<Task>>().ToList();
            var elementToRemove = elements.FirstOrDefault(cl => cl.DB_ID == task.DB_ID);
            elements.Remove(elementToRemove);
            storage.Save<List<Task>>(elements);
        }
    }
}
