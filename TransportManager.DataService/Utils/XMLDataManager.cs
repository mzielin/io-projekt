﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TransportManager.DataService.Utils
{
    public class XMLDataManager
    {
        private string storagePath;
        public XMLDataManager (string dataStoragePath)
        {
            storagePath = @"C:\Users\pinki_000\" + dataStoragePath;
        }
        public void Save<T>(T toSave)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            TextWriter textWriter = new StreamWriter(storagePath);
            serializer.Serialize(textWriter, toSave);
            textWriter.Close();
        }
        public T Load<T>()
        {
            //if (!File.Exists(storagePath)) File.Create(storagePath).Close();
            XmlSerializer deserializer = new XmlSerializer(typeof(T));
            TextReader textReader = new StreamReader(storagePath);
            T data = (T)deserializer.Deserialize(textReader);
            textReader.Close();
            return data;
        }

        public void AddSingle<T>(T e)
        {
            var elements = Load<List<T>>();
            elements.Add(e);
            Save<List<T>>(elements);
        }
    }
}
