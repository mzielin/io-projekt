﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransportManager.DataService.Implementations;
using TransportManager.DataService.Interfaces;

namespace TransportManager.DataService
{
    public class DataService : IDataService
    {
        private IClientRepository clients = new ClientRepository();
        private IDispatchRepository dispatches = new DispatchRepository();
        private IDriverRepository drivers = new DriverRepository();
        private ITaskRepository tasks = new TaskRepository();
        private ITrailerRepository trailers = new TrailerRepository();
        private IVehicleRepository vehicles = new VehicleRepository();
        private IWarehouseRepository warehouses = new WarehouseRepository();
        private IPOIRepository pois = new POIRepository();
        public DataService()
        {


        }

        public Model.Client AddClient(Model.Client client)
        {
            return clients.AddClient(client);
        }

        public IEnumerable<Model.Client> GetAllClients()
        {
            return clients.GetAllClients();
        }

        public Model.Client GetClientById(string id)
        {
            return clients.GetClientById(id);
        }

        public void RemoveClient(Model.Client client)
        {
            clients.RemoveClient(client);
        }

        public Model.Dispatch AddDispatch(Model.Dispatch dispatch)
        {
            return dispatches.AddDispatch(dispatch);
        }

        public IEnumerable<Model.Dispatch> GetAllDispatches()
        {
            return dispatches.GetAllDispatches();
        }

        public void RemoveDispatch(Model.Dispatch dispatch)
        {
            dispatches.RemoveDispatch(dispatch);
        }

        public Model.Driver AddDriver(Model.Driver driver)
        {
            return drivers.AddDriver(driver);
        }

        public IEnumerable<Model.Driver> GetAllDrivers()
        {
            return drivers.GetAllDrivers();
        }

        public Model.Driver GetDriverById(string id)
        {
            return drivers.GetDriverById(id);
        }

        public void RemoveDriver(Model.Driver driver)
        {
            drivers.RemoveDriver(driver);
        }

        public Model.Warehouse AddWarehouse(Model.Warehouse warehouset)
        {
            return warehouses.AddWarehouse(warehouset);
        }

        public IEnumerable<Model.Warehouse> GetAllWarehouses()
        {
            return warehouses.GetAllWarehouses();
        }

        public void RemoveWarehouse(Model.Warehouse warehouse)
        {
            warehouses.RemoveWarehouse(warehouse);
        }

        public Model.Vehicle AddVehicle(Model.Vehicle vehicle)
        {
            return vehicles.AddVehicle(vehicle);
        }

        public IEnumerable<Model.Vehicle> GetAllVehicles()
        {
            return vehicles.GetAllVehicles();
        }

        public void RemoveVehicle(Model.Vehicle vehicle)
        {
            vehicles.RemoveVehicle(vehicle);
        }

        public Model.Trailer AddTrailer(Model.Trailer trailer)
        {
            return trailers.AddTrailer(trailer);
        }

        public IEnumerable<Model.Trailer> GetAllTrailers()
        {
            return trailers.GetAllTrailers();
        }

        public void RemoveTrailer(Model.Trailer trailer)
        {
            trailers.RemoveTrailer(trailer);
        }

        public Model.Task AddTask(Model.Task task)
        {
            return tasks.AddTask(task);
        }

        public IEnumerable<Model.Task> GetAllTasks()
        {
            return tasks.GetAllTasks();
        }

        public Model.Task GetTaskById(string id)
        {
            return tasks.GetTaskById(id);
        }

        public void RemoveTask(Model.Task task)
        {
            tasks.RemoveTask(task);
        }

        public Model.POI AddPOI(Model.POI poi)
        {
            return pois.AddPOI(poi);
        }

        public IEnumerable<Model.POI> GetAllPOIs()
        {
            return pois.GetAllPOIs();
        }

        public void RemovePOI(Model.POI poi)
        {
            pois.RemovePOI(poi);
        }
    }
}
